package com.symphony.reactive.controller;

import org.junit.Test;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

import java.time.Duration;

public class ReactorStepsTest {


	@Test
	public void shouldAddAnumberOnEnd(){
		Flux<Integer> starting=Flux.just(1,2);
		StepVerifier.create(
				new FluxEnchancer().addNumber(starting,5))
					.expectNext(1)
					.expectNext(2)
					.expectNext(5)
					.verifyComplete();
	}


	@Test
	public void shouldBeAbleToRunWithDelay(){
		Flux<Integer> starting=Flux.just(1,2);
		StepVerifier.withVirtualTime(
				() -> starting.delayElements(Duration.ofSeconds(100))).expectSubscription()
					.expectNoEvent(Duration.ofSeconds(100))
					.expectNext(1)
					.expectNoEvent(Duration.ofSeconds(100))
					.expectNext(2).verifyComplete();
	}


	@Test
	public void shouldBeAbleToRunWithDelayNoVirtualTime(){
		Flux<Integer> starting=Flux.just(1,2);
		StepVerifier.create(
				starting.delayElements(Duration.ofSeconds(5))).expectSubscription()
					.expectNoEvent(Duration.ofSeconds(5))
					.expectNext(1)
					.expectNoEvent(Duration.ofSeconds(5))
					.expectNext(2).verifyComplete();
	}
}
