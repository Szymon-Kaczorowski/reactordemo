package com.symphony.reactive.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;

@SpringBootTest
@RunWith(SpringRunner.class)
@AutoConfigureWebTestClient
public class AsynchronousHelloTest {


	@Autowired
	WebTestClient webTestClient;
	@Test
	public void getSingle() {
		//WHEN
		final WebTestClient.ResponseSpec exchange = webTestClient.get().uri("/echo2").exchange();
		//THEN
		exchange.expectStatus()
				.is2xxSuccessful()
				.expectBody()
				.isEmpty();

	}
}