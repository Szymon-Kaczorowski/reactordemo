package com.symphony.reactive;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ReacitveApplication {

	public static void main(String[] args) {
		SpringApplication.run(ReacitveApplication.class, args);
	}
}
