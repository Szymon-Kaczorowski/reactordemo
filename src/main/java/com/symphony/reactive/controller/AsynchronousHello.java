package com.symphony.reactive.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Duration;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.IntStream;

import static java.util.Collections.singletonMap;

@RestController
@Slf4j
public class AsynchronousHello {

	@GetMapping(value = "/echo", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public Flux<Map<String, String>> getHellos() {
		log.info("before creating flux");
		final Flux<Map<String, String>> mapFlux = Flux.just(
				singletonMap("1", "1"),
				singletonMap("2", "2"),
				singletonMap("3", "4"),
				singletonMap("4", "5")
		)
													  .delayElements(Duration.ofSeconds(1));
		log.info("after creating flux");

		return mapFlux;
	}

	@GetMapping(value = "/echo2")
	public Mono<Map<String, String>> getSingle() {

		return Mono.empty();
	}

	@GetMapping(value = "/zip")
	public Flux joinTwoStreams() {
		final Flux<String> first = Flux.just("1", "2", "3");
		final Flux<String> second = Flux.just("3", "4", "5");
		return Flux.zip(first, second);

	}

	@GetMapping(value = "/latest")
	public Flux joinTwoStreamsLatest() {
		final Flux<String> first = Flux.just("1", "2", "3").delayElements(Duration.ofMillis(5));
		final Flux<String> second = Flux.just("3", "4", "5").delayElements(Duration.ofMillis(6));

		return Flux.combineLatest(Function.identity(), first, second);

	}


	@GetMapping(value = "/sample")
	public Flux<Integer> sample() {
		final Flux<Integer> integerFlux = Flux.fromStream(IntStream.range(0, 100).boxed())
											  .delayElements(Duration.ofMillis(200));
		return Flux.from(integerFlux).sample(Duration.ofSeconds(2));


	}

	@GetMapping(value = "/take")
	public Flux<Integer> take() {
		final Flux<Integer> integerFlux = Flux.fromStream(IntStream.range(0, 100).boxed()).delayElements(Duration.ofMillis(30));

		return Flux.from(integerFlux).take(Duration.ofSeconds(2));

	}

	@GetMapping("/window")
	public Flux<Integer> window() {
		final Flux<Integer> integerFlux = Flux.fromStream(IntStream.range(0, 100).boxed()).delayElements(Duration.ofMillis(30));

		return Flux.from(integerFlux)
				   .window(Duration.ofSeconds(1))
				   .skip(1)
				   .take(1)
				   .flatMap(Function.identity());
	}

	@GetMapping("/concat")
	public Flux<Integer> concat() {
		final Flux<Integer> slowStream = Flux.fromStream(IntStream.range(0, 100).boxed()).delayElements(Duration.ofMillis(20));
		final Flux<Integer> fastStream = Flux.fromStream(IntStream.range(100, 200).boxed()).delayElements(Duration.ofMillis(5));

		return Flux.concat(slowStream, fastStream);
	}

	@GetMapping("/merge")
	public Flux<Map<String, Integer>> merge() {
		final Flux<Integer> slowStream = Flux.fromStream(IntStream.range(0, 100).boxed()).delayElements(Duration.ofMillis(20));
		final Flux<Integer> fastStream = Flux.fromStream(IntStream.range(100, 200).boxed()).delayElements(Duration.ofMillis(5));

		return Flux.merge(slowStream, fastStream).map(elem -> singletonMap("data", elem));
	}

	@GetMapping("/first")
	public Flux<Integer> first() {
		final Flux<Integer> slowStream = Flux.fromStream(IntStream.range(0, 100).boxed()).delayElements(Duration.ofMillis(20));
		final Flux<Integer> fastStream = Flux.fromStream(IntStream.range(100, 200).boxed()).delayElements(Duration.ofMillis(5));
		return Flux.first(slowStream, fastStream);
	}


	@GetMapping("/first2")
	public Flux<Integer> first2() {
		final Flux<Integer> slowStream = Flux.fromStream(IntStream.range(0, 100).boxed()).delayElements(Duration.ofMillis(20));
		final Flux<Integer> fastStream = Flux.fromStream(IntStream.range(100, 200).boxed())
											 .delayElements(Duration.ofMillis(5))
											 .delaySequence(Duration.ofMillis(17));

		return Flux.first(slowStream, fastStream);
	}


}
