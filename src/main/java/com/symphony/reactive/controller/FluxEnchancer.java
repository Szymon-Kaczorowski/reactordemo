package com.symphony.reactive.controller;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public class FluxEnchancer {


	public Flux<Integer> addNumber(Flux<Integer> current, int nextVal){
		return current.concatWith(Mono.just(nextVal));

	}
}
